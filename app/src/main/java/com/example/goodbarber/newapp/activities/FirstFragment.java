package com.example.goodbarber.newapp.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.goodbarber.newapp.R;
import com.example.goodbarber.newapp.adapter.Article;
import com.example.goodbarber.newapp.adapter.RecyclerViewAdapter;
import java.util.ArrayList;
import java.util.Objects;

public class FirstFragment extends Fragment
{
    private RecyclerView        mRecyclerView;
    private ArrayList<Article>  mCities = new ArrayList<>();
    private ArticleViewmodel    mArticleViewmodel;
    private RecyclerViewAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState)
    {
        View v = inflater.inflate( R.layout.recycler_view, container, false );


        mArticleViewmodel =ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ArticleViewmodel.class);
        mRecyclerView = v.findViewById(R.id.cities);

        mAdapter = new RecyclerViewAdapter( this, mCities, mArticleViewmodel );
        mRecyclerView.setAdapter( mAdapter );
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        Log.d("log", "start");
        ArticleViewmodel viewmodel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ArticleViewmodel.class);

        viewmodel.getSelected2().observe(this, new Observer<ArrayList<Article>>()
        {
            @Override
            public void onChanged(@Nullable ArrayList<Article> cityArrayList)
            {
                mAdapter.update( cityArrayList);
            }
        });

        return v;
    }

}

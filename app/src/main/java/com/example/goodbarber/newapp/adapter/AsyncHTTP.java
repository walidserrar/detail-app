package com.example.goodbarber.newapp.adapter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import com.example.goodbarber.newapp.activities.ArticleViewmodel;
import com.example.goodbarber.newapp.activities.LoadJsonListener;
import com.example.goodbarber.newapp.model.MyApplication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncHTTP extends AsyncTask<String, String, String>
{

    private final WeakReference<ArticleViewmodel> mArticleViewmodelWeakReference;
    private LoadJsonListener mListener;

    public AsyncHTTP(LoadJsonListener loadJsonListener, ArticleViewmodel weakRef)
    {
        mListener = loadJsonListener;
        mArticleViewmodelWeakReference = new WeakReference<>(weakRef);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        //ArticleViewmodel articleViewmodel = ViewModelProviders.of().get( ArticleViewmodel.class );         //TODO : weakreference, Verification of pathFile exist, and if not create a pop up !
        //new AccesStorage().readInternalStorage( "jsonData" );                                              //TODO I started it and I'll finish Monday !!
    }

    protected String doInBackground(String... params)
    {
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try
        {
            URL url = new URL( params[0] );
            if (isOnline())
            {
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
            }
            else
            {
                return null;
            }
            InputStream file = connection.getInputStream();
            reader = new BufferedReader( new InputStreamReader( file ) );
            StringBuffer buffer = new StringBuffer();
            String line;

            while ((line = reader.readLine()) != null)
            {
                buffer.append( line ).append( "\n" );
                Log.d( "reponse", "> " + line );
            }

            Log.d( "cio", buffer.toString() );
            new AccesStorage().writeInternalStorage( "jsonData", buffer.toString() );

            return buffer.toString();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (connection != null)
            {
                connection.disconnect();
            }
            try
            {
                if (reader != null)
                {
                    reader.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s)
    {
        super.onPostExecute( s );
        if (s != null)
        {
            mListener.notifySucess();
        }
        else
        {
            mListener.notifyFailure();
        }
    }



    private Boolean isOnline()
    {
        NetworkInfo network = ((ConnectivityManager) MyApplication.getAppContext().getSystemService( Context.CONNECTIVITY_SERVICE )).getActiveNetworkInfo();
        boolean a = network != null && network.isConnected();
        return network != null && network.isConnected();
    }
}
package com.example.goodbarber.newapp.model;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

/**
 * Created by goodbarber on 2019-07-10
 * All rights reserved GoodBarber
 */
public class MyApplication extends Application
{
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public void onCreate(){
        super.onCreate();
        MyApplication.context=getApplicationContext();
    }

    public static Context getAppContext(){
        return MyApplication.context;
    }
}

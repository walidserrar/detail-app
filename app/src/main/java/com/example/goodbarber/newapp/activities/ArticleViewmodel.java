package com.example.goodbarber.newapp.activities;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import com.example.goodbarber.newapp.adapter.AccesStorage;
import com.example.goodbarber.newapp.adapter.AsyncHTTP;
import com.example.goodbarber.newapp.adapter.Article;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by goodbarber on 2019-07-12
 * All rights reserved GoodBarber
 */
public class ArticleViewmodel extends ViewModel implements LoadJsonListener
{
    private final MutableLiveData<Article>            selected = new MutableLiveData<>();
    private       MutableLiveData<ArrayList<Article>> list     = new MutableLiveData<>();
    private       MutableLiveData<AccesStorage>       storage  = new MutableLiveData<>();


    public  void select(Article item) {
        Log.d( "log", item.getContent() );
        selected.setValue(item);
    }                               //TODO : GetData

    public LiveData<Article> getSelected() {
        return selected;
    }


    public void select2(ArrayList<Article> item) {
        Log.d( "log", String.valueOf( item ) );
        list.setValue(item);
    }                               //TODO : GetData

    public LiveData<ArrayList<Article>> getSelected2() {
        list = new MutableLiveData<>();
        return list;
    }
    public void storageEmpty()
    {
        storage.postValue( null );
    }
    public LiveData<AccesStorage> getStorageInfo(){
        return storage;
    }


    public void startAsync() {

        new AsyncHTTP(this).execute( "Https://api.goodbarber.net/front/get_items/939101/14879715/?local=1" );
    }

    @Override
    public void notifySucess()
    {
        String s = new AccesStorage().readInternalStorage( "jsonData" );
        select2(stringToArray( s ));
    }

    @Override
    public void notifyFailure()
    {
        String s = new AccesStorage().readInternalStorage( "jsonData" );
        select2(stringToArray( s ));
    }

// this function transform a string to an ArrayList

    private ArrayList<Article> stringToArray(String data){
        JSONObject json = null;
        try {
            json=new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<Article> arrayArticleOverviewClass = new ArrayList<>();
        for (int i=0; i<10; i++){
            Article articleOverviewClass = null;
            try {
                if (json != null)
                {
                    articleOverviewClass = new Article( json.getJSONArray( "items").getJSONObject( i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            arrayArticleOverviewClass.add(articleOverviewClass);
        }
        return arrayArticleOverviewClass;
    }
}


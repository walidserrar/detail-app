package com.example.goodbarber.newapp.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.example.goodbarber.newapp.R;
import com.example.goodbarber.newapp.adapter.Article;

public class MainActivity extends AppCompatActivity
{

    private Fragment       mFragmentA;
    private SecondFragment mFragmentB;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.activity_main);
        mFragmentA = new FirstFragment();

        if((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL)   // Take info of screen device
        {                                                                                                                                       //  phone
            //Log.d("log", "I'm in");
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add( R.id.cA, mFragmentA );                                                     //build first fragment  ( fragmentAccueil )
            fragmentTransaction.commit();

            ArticleViewmodel model = ViewModelProviders.of( this).get( ArticleViewmodel.class);                 //ViewModel
            model.startAsync();                                                     // Start asyncTack
            model.getSelected().observe( this, new Observer<Article>()
            {
                @Override
                public void onChanged(@Nullable Article city)
                {
                    Log.i("log", "onChanged City");
                    exchange( city );
                }
            } );
        }

        if((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE)         //Take information of screen device
        {                                                                                                                                             // if the device is a Tablet
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add( R.id.container1, mFragmentA );                             // build First fragment  ( fragmentAccueil )
            fragmentTransaction.commit();

            ArticleViewmodel model = ViewModelProviders.of( this).get( ArticleViewmodel.class );   //   ViewModel
            model.startAsync();                                                                     //start my AsyncTask
            model.getSelected().observe( this, new Observer<Article>()
            {
                @Override
                public void onChanged(@Nullable Article city)
                {
                    bothFragment( city );
                }
            } );            //My Observer
        }
        }

    public void bothFragment(Article city){
        //method for Tablet
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if(mFragmentB!= null){
            fragmentTransaction.remove( mFragmentB );
        }
        mFragmentB= (SecondFragment) new SecondFragment().newInstance( city );
        //display the second fragment
        fragmentTransaction.add( R.id.container2,mFragmentB );
        fragmentTransaction.commit();

    }

    public void exchange(Article city)
    {
        //method for phone
        ArticleViewmodel model = ViewModelProviders.of( this ).get( ArticleViewmodel.class );
        mFragmentB= new SecondFragment().newInstance( city);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove( mFragmentA );    //remove the first fragment
        fragmentTransaction.add( R.id.cB,mFragmentB );   // add the second fragment      i work with 2 relative layout
        //fragmentTransaction.replace( R.id.cA, mFragmentB);
        fragmentTransaction.addToBackStack("return"); // the back button
        fragmentTransaction.commit();
        model.startAsync();                                                 //restart the asyncTask to read the cache
    }
}
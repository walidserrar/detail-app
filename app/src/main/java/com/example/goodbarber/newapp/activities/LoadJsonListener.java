package com.example.goodbarber.newapp.activities;

/**
 * Created by goodbarber on 2019-07-09
 * All rights reserved GoodBarber
 */
public interface LoadJsonListener
{

    void notifySucess();
    void notifyFailure();
}

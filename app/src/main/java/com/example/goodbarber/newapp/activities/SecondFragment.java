package com.example.goodbarber.newapp.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.example.goodbarber.newapp.R;
import com.example.goodbarber.newapp.adapter.Article;

public class SecondFragment extends Fragment
{
    private Article city;
    public SecondFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState)
    {
        View v = inflater.inflate( R.layout.activity_details, container, false);

        final WebView webView3=new WebView( getActivity());
        webView3.loadDataWithBaseURL("","<style>img{display: inline;height: auto;max-width: 100%;}</style>" +city.getContent(), "text/html", "UTF-8", null);
        Log.d( "log", "le city du detail :"+city.getContent() );
        final RelativeLayout relativeLayout = v.findViewById( R.id.detailRelative);
        relativeLayout.addView(webView3);
        return v;
    }

    public SecondFragment newInstance(Article article )
{
    SecondFragment fragment = new SecondFragment();
    Bundle args = new Bundle();
    if(article!=null)
        args.putSerializable( "article", article );
    //Log.d( "log", "Value of args :"+article );
    fragment.setArguments( args );
    return fragment;
}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate( savedInstanceState );
        if (getArguments() != null)
        {
            city = (Article) getArguments().getSerializable( "article");

            //Log.d( "log1", String.valueOf( getArguments() ) );
        }
    }
}

package com.example.goodbarber.newapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.goodbarber.newapp.R;
import com.example.goodbarber.newapp.activities.ArticleViewmodel;
import com.example.goodbarber.newapp.activities.FirstFragment;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
{

    private ArrayList<Article> mCities;
    private FirstFragment      activities;
    private Article            city2;
    private ArticleViewmodel   mArticleViewmodel;


    public RecyclerViewAdapter(FirstFragment activities, ArrayList<Article> cities, ArticleViewmodel articleViewmodel)
    {
        Log.d("log", "create adapter");
        this.mCities=cities;
        this.activities=activities;
        this.mArticleViewmodel = articleViewmodel;
    }

    public void update(ArrayList<Article> cityArrayList){
        mCities=cityArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cities, parent, false);

        return new RecyclerViewAdapter.ViewHolder( v);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position)
    {

        //update( city2 );
        city2 = mCities.get( position);
        ViewHolder h = (ViewHolder) holder;
        Log.d("log", "je suis dans mon onBind");

        h.mTitre.setText(city2.getTitle());
        h.mDate.setText(city2.getDate());


        try{
            Picasso.get().load( city2.getImageURL()).into(h.image);
        }catch (Exception e){
            e.printStackTrace();
        }


        h.image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                    mArticleViewmodel.select( mCities.get( position ));
                    Log.d( "log", String.valueOf( city2 ) );
                    //cityViewmodel.startAsync();
            }
        });


     }



    @Override
    public int getItemCount()
    {
        if (mCities != null){
            return mCities.size();
        }
        return 0;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        final View      view;
        final ImageView image;
        final TextView  mDate;
        final TextView  mTitre;

        private ViewHolder(View view){
            super(view);
            this.view= view;
            image = view.findViewById(R.id.image);
            mTitre = view.findViewById(R.id.Titre);
            mDate = view.findViewById(R.id.accueil_date);
        }

    }
}
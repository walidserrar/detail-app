package com.example.goodbarber.newapp.adapter;

import android.content.Context;
import com.example.goodbarber.newapp.model.MyApplication;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by goodbarber on 2019-07-09
 * All rights reserved GoodBarber
 */
public class AccesStorage
{

    public String writeInternalStorage(String jsonData, String Donnee)
    {
        try
        {
            FileOutputStream outputStream = MyApplication.getAppContext().openFileOutput( jsonData, Context.MODE_PRIVATE );
            outputStream.write(Donnee.getBytes());
            outputStream.close();
            //Log.d( "file", "file created and written" );
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return jsonData;
    }

    public String readInternalStorage(String name)
    {
        FileInputStream is = null;
        try{
           // Log.d("file", "read the cache ");
            is = MyApplication.getAppContext().openFileInput(name);
            InputStreamReader inputStreamReader = new InputStreamReader( is );
            BufferedReader bufferedReader = new BufferedReader( inputStreamReader );
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while((line = bufferedReader.readLine())!= null){
                stringBuilder.append( line );
            }
            return stringBuilder+"";
        }catch (FileNotFoundException e){
            /*AlertDialog.Builder builder = new AlertDialog.Builder( MyApplication.getAppContext() );

            builder.setTitle( "Empty Data" )
                    .setMessage( "No data on your cache memory" )
                    .setPositiveButton( "Quit", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int which)
                        {

                        }
                    } ).create()
                    .show();*/
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}

package com.example.goodbarber.newapp.adapter;

import android.annotation.SuppressLint;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
     * Created by goodbarber on 2019-07-02      fait
     * All rights reserved GoodBarber
 */
public class Article extends ArrayList<com.example.goodbarber.newapp.adapter.Article> implements Serializable
{

    private String imageURL;
    private String sDate;
    private String mTitle;
    private String Auteur;
    private String Article;
    private String mSecondaryTitle;

    public Article(JSONObject jsonObject)
    {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //Constructor
        try
        {
            this.imageURL=jsonObject.getJSONArray("images").getJSONObject(0).getString("url");
            this.mTitle =jsonObject.getString( "title");
            Date newDate = dateFormat.parse( jsonObject.getString( "date" ) );
            this.sDate = dateFormat.format(newDate);
            this.Auteur=jsonObject.getString("author");
            this.Article=jsonObject.getString("content");
            this.mSecondaryTitle=jsonObject.getString("summary");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public String getContent(){
        return getSecondaryTitle()+getDate()+getAuteur()+getArticle();
    }


    public String getTitle(){return mTitle;}

    public String getImageURL(){return imageURL;}
    public String getDate() { return sDate;}

    private String getAuteur() {return Auteur;}
    private String getArticle() {return Article;}
    private String getSecondaryTitle() {return mSecondaryTitle;}

}